import 'dart:ui';

class MyColors {
  static const grayText = const Color(0xff626365);
  static const grayText1 = const Color(0xff626365);
  static const greenButton = const Color(0xff37A65E);
}