import 'package:cloud_firestore/cloud_firestore.dart';

class UserData {


  DocumentReference reference;
  String nickName;
  String realFirstName;
  String realFirst2Name;
  String realLastName;
  String realLast2Name;
  String email;
  String mobilePhone;
  String photoString;
  String numberId;
  DocumentReference typeNumberId;

  UserData.fromJson(Map<String, dynamic> json, DocumentReference reference) {
    this.reference = reference;
    this.nickName = json["nickName"] ?? "";
    this.realFirstName = json["realFirstName"] ?? "";
    this.realFirst2Name = json["realFirst2Name"] ?? "";
    this.realLastName = json["realLastName"] ?? "";
    this.realLast2Name = json["realLast2Name"] ?? "";
    this.email = json["email"] ?? "";
    this.mobilePhone = json["mobilePhone"] ?? "";
    this.photoString = json["photoString"] ?? "";
    this.numberId = json["numberId"] ?? "";
    this.typeNumberId = json["typeNumberId"];
  }

  Map<String, dynamic> toJson() => {
      "nickName":nickName,
    "realFirstName":realFirstName,
    "realFirst2Name":realFirst2Name,
    "realLastName":realLastName,
    "realLast2Name":realLast2Name,
      "email":email,
      "mobilePhone":mobilePhone,
      "photoString":photoString,
      "numberId":numberId,
      "typeNumberId":typeNumberId

    };

  UserData(
  {this.reference,
      this.nickName,
      this.realFirstName,
      this.realFirst2Name,
      this.realLastName,
      this.realLast2Name,
      this.email,
      this.mobilePhone,
      this.photoString,
      this.numberId,
      this.typeNumberId});
}