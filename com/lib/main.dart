import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

import 'RouterGenerator.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'Google SignIn';

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Muli',
        brightness: Brightness.light,
      ),
      onGenerateRoute: RouterGenerator.generateRoute,
    );

  }
}