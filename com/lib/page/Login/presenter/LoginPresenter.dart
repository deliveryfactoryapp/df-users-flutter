import 'package:com/Data/UserData.dart';
import 'package:com/page/Login/iterator/LoginInteractor.dart';
import 'package:com/page/Login/view/Login.dart';
import 'package:firebase_auth/firebase_auth.dart';

abstract class OnLoginPresenter {
  void onSuccessGetUser(UserData userData);
  void onNoUserLogin();
  void onError(String error);
}

class LoginPresenter {
  OnLoginPresenter onLoginPresenter;
  LoginIterator loginIterator;

  LoginPresenter(this.onLoginPresenter) {
    loginIterator = LoginIterator(this);
  }


  void login() => loginIterator.login();
  void getUser() => loginIterator.getUser();


  void onSuccessGetUser(UserData userData) => onLoginPresenter.onSuccessGetUser(userData);
  void onNoUserLogin() => onLoginPresenter.onNoUserLogin();
  void onError(String error) => onLoginPresenter.onError(error);

}
