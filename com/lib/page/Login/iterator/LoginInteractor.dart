
import 'package:com/Repository/LoginRepository.dart';
import 'package:com/page/Login/presenter/LoginPresenter.dart';
import 'package:firebase_auth/firebase_auth.dart';

abstract class OnLoginIterator {
  void onSuccessLogin(User user);
  void onError(String error);
}

class LoginIterator {

  LoginPresenter loginPresenter;
  LoginRepository loginRepository;

  LoginIterator(this.loginPresenter) {
    loginRepository = LoginRepository(loginPresenter);
  }

  void login() => loginRepository.login();
  void getUser() => loginRepository.getUser();

}