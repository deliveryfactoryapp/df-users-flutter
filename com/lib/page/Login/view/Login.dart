import 'package:com/Data/UserData.dart';
import 'package:com/page/DialogBasicData/View/FormBasicData.dart';
import 'package:com/page/Login/presenter/LoginPresenter.dart';
import 'package:flutter/material.dart';
import 'package:com/widget/buttonFb.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Login();
}

class _Login extends State<Login> implements OnLoginPresenter {
  bool isLoading = false;

  LoginPresenter loginPresenter;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      isLoading = true;
    });
    loginPresenter = LoginPresenter(this);
    loginPresenter.getUser();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      body: Container(
        // margin: EdgeInsets.all(0),
        child: Column(
          children: [
            getImage(),
            SizedBox(height: 80),
            getButtonGoogle(),
            SizedBox(height: 80),
            ButtonFb()
          ],
        ),
      ),
    );
  }

  Widget getImage() {
    return Container(
        margin: EdgeInsets.only(top: 86, left: 108, right: 108),
        child: Image.asset("assets/logoDF.png"));
  }

  Widget getButtonGoogle() {
    return (isLoading)
        ? Container(
            margin: EdgeInsets.only(left: 36, right: 36),
            height: 45,
            child: CircularProgressIndicator(),
          )
        : GestureDetector(
            child: Container(
                margin: EdgeInsets.only(left: 36, right: 36),
                child: Image.asset("assets/buttonGoogle.png")),
            onTap: () {
              setState(() {
                isLoading = true;
              });

              loginPresenter.login();
            },
          );
  }

  @override
  void onError(String error) {
    setState(() {
      isLoading = false;
    });
  }

  @override
  void onNoUserLogin() {
    setState(() {
      isLoading = false;
    });
  }

  @override
  void onSuccessGetUser(UserData userData) {
    setState(() {
      isLoading = false;
    });

    if (userData.email == null ||
        userData.nickName == null ||
        userData.mobilePhone == "" ||
        userData.email == "" ||
        userData.nickName == "" ||
        userData.mobilePhone == "") {
      showDialogFormBasicData(userData);
      return;
    }
    Navigator.pushNamed(context, "/Categories", arguments: userData);
  }

  showDialogFormBasicData(UserData userData) {
    showDialog(
      context: context,
      builder: (BuildContext context) => FormBasicData(
        userData: userData,
        context: context,
        onSuccessRegister: (userData) {
          Navigator.pushNamed(context, "/Categories", arguments: userData);
        },
      ),
    );
  }
}
