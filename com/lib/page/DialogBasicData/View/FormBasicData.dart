import 'package:com/Data/UserData.dart';
import 'package:com/Utils/Constans.dart';
import 'package:com/Utils/MyColors.dart';
import 'package:com/widget/BasicWidgetPage.dart';
import 'package:com/widget/BuildFormWithValidation.dart';
import 'package:com/widget/CustomButton.dart';
import 'package:com/widget/GeneralText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FormBasicData extends StatefulWidget{
  UserData userData;
  BuildContext context;
  Function(UserData) onSuccessRegister;

  FormBasicData({this.userData, this.context, this.onSuccessRegister});

  @override
  State<StatefulWidget> createState() => _FormBasicData();

}

class _FormBasicData extends State<FormBasicData>{
  Constans constans;
  List<BuildForm> buildForms = [BuildForm(hint: "Name:", maxChars: 30, minChars: 1, typeField: TypeField.alpha), BuildForm(hint: "Email:", maxChars: 30, minChars: 1, typeField: TypeField.email), BuildForm(hint: "mobile:", maxChars: 12, minChars: 1, typeField: TypeField.number)];

  bool isEnabledButton = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double height = MediaQuery.of(context).size.height;
    constans = Constans();

    return Container(
      margin: EdgeInsets.all(30),
      child: Scaffold(
       body: Container(
         height: height * 0.8,
         child: ListView(
           children: [
             getTitleText(),
             SizedBox(height: 30,),
             getSubTitleText(),
             getFormTextField(constans.name),
             SizedBox(height: 30,),
             getContainerButton()

           ],

         ),
       ),
      ),
    );
  }

  Widget getContainerButton(){
    return Container(

      margin: EdgeInsets.only(
          right: 20,
          left: 20
      ),
      child: CustomButton(
        onTap: (){

        },
        height: 47,
        enabled: isEnabledButton,
        isLoading: false,
        text: Constans().continuee,
      ),
    );
  }

  Widget getTitleText() {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(
        top: 71,
        left: 20,
        right: 20

      ),
      child: GeneralText().getGeneralText(
          text: constans.yourUser,
          fontFamily: FontFamily.regular,
          fontSize: 30,
          color: MyColors.grayText1
      ),
    );
  }

  Widget getSubTitleText() {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(
        left: 20,
        right: 20
      ),
      child: GeneralText().getGeneralText(
          text: constans.titleDialogFormBasicData,
          fontFamily: FontFamily.regular,
          fontSize: 15,
          color: MyColors.grayText
      ),
    );
  }

  Widget getFormTextField(String text){
    return Container(
      margin: EdgeInsets.only(
          left: 20,
          right: 20
      ),
      child: BuildFormWithValidation(buildForms: buildForms,
      onValidation: (validate){
        setState(() {
          isEnabledButton = validate;
        });

      },)
    );
  }

}