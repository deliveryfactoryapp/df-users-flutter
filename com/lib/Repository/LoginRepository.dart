import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:com/Data/UserData.dart';
import 'package:com/page/Login/presenter/LoginPresenter.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

abstract class OnLoginRepository {
  void onSuccessLogin(UserData userData);
  void onError(String error);
}

class LoginRepository {
  LoginPresenter loginPresenter;

  LoginRepository(this.loginPresenter);

  String referenceUser = "Users";

  FirebaseFirestore firestore = FirebaseFirestore.instance;

  void getUser() {
    final user = FirebaseAuth.instance.currentUser;
    if (user == null){
      loginPresenter.onLoginPresenter.onNoUserLogin();
      return;
    }
    firestore.collection(referenceUser).where("email", isEqualTo: user.email).get().
    then((value) {
      if (value.docs.length >= 1) {
        UserData userData = UserData.fromJson(value.docs.first.data(), value.docs.first.reference);
        loginPresenter.onSuccessGetUser(userData);

      } else {
        DocumentReference reference =  firestore.collection(referenceUser).doc(user.uid);
        UserData userData = UserData(
          email: user.email,
          mobilePhone: "",
          nickName: user.displayName,
          photoString: user.photoURL,
          reference: reference
        );

        reference.set(userData.toJson()).then((value) {
          loginPresenter.onSuccessGetUser(userData);
        }).
        catchError((err){
          loginPresenter.onError(err.toString());
        });


      }
    }).
    catchError((err){
      loginPresenter.onError(err.toString());
    });
  }
  
  void login() async {
    final userGoogle = await GoogleSignIn().signIn();
    if (userGoogle == null) {
      final user = FirebaseAuth.instance.currentUser;

    } else {
      final googleAuth = await userGoogle.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      await FirebaseAuth.instance.signInWithCredential(credential);


      getUser();

    }
  }

  void logout() async {
    await GoogleSignIn().disconnect();
    FirebaseAuth.instance.signOut();
  }
}