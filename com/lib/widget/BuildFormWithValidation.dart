import 'package:com/widget/CustomButton.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


enum TypeField {text, alpha, alphaNumeric, number, email, amount, upper, lower}

class BuildForm{
  String hint;
  int maxChars = 40;
  int minChars = 1;
  bool isEnabled = false;
  TypeField typeField = TypeField.text;
  TextEditingController textEditingController = TextEditingController();


  BuildForm({this.hint, this.maxChars, this.minChars, this.typeField});
}

class BuildFormWithValidation extends StatefulWidget{
  List<BuildForm> buildForms;
  Function(bool) onValidation;

  BuildFormWithValidation({this.buildForms, this.onValidation});

  @override
  State<StatefulWidget> createState() => _BuildFormWithValidation();

}

class _BuildFormWithValidation extends State<BuildFormWithValidation> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
        child: Column(

          children: getForms(),
        )
    );
  }

  List<Widget> getForms() {
    List<Widget> widgets = List<Widget>();

    widget.buildForms.forEach((element) {
      List<TextInputFormatter> textInputs = List<TextInputFormatter>();

      TextInputType textInputType = TextInputType.text;
      switch(element.typeField){
        case TypeField.alpha:
          textInputs.add(WhitelistingTextInputFormatter(RegExp("[a-zA-Z ]")));
          textInputType = TextInputType.text;
          break;
        case TypeField.alphaNumeric:
          textInputs.add(WhitelistingTextInputFormatter(RegExp("[a-zA-Z 0-9]")));
          textInputType = TextInputType.text;
          break;
        case TypeField.number:
          textInputs.add(WhitelistingTextInputFormatter(RegExp("[0-9]")));
          textInputType = TextInputType.number;
          break;
        case TypeField.email:
          textInputType = TextInputType.emailAddress;
          break;
      }
      Widget widget = Container(

        margin: EdgeInsets.only(
            top: 14,
        ),
        child: TextFormField(
          keyboardType: textInputType,
          controller: element.textEditingController,
          inputFormatters: textInputs,
          onChanged: (value){

            element.isEnabled = (element.textEditingController.text.length >= element.minChars && element.textEditingController.text.length <= element.maxChars);
            bool Validation = (this.widget.buildForms.where((en) {

              return !en.isEnabled;
            }).length >= 1);
            this.widget.onValidation(!Validation);

          },
          decoration: InputDecoration(
            labelText: element.hint,
          ),
        ),
      );


      widgets.add(widget);
    });



    return widgets;
  }

  Widget getFormTextField(String text){
    return Container(
      margin: EdgeInsets.only(
          left: 20,
          right: 20
      ),
      child: TextFormField(

        decoration: InputDecoration(
          labelText: text,

        ),
      ),
    );
  }

}