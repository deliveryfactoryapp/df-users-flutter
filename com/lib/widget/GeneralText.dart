

import 'package:flutter/cupertino.dart';

enum FontFamily {regular, blond, medium}
class GeneralText {
  Widget getGeneralText({String text, double height, Color color, double fontSize, FontFamily fontFamily}) {

    String font = "Roboto-Regular";
    switch(fontFamily) {
      case FontFamily.regular:
        font = "Roboto-Regular";
        break;
      case FontFamily.blond:
        font = "Roboto-Bold";
        break;
      case FontFamily.medium:
        font = "Roboto-Medium";
        break;
    }
    return Text(text,
    style: TextStyle(
        color: color,
      fontSize: fontSize,
      fontFamily:font
    ),
    );
  }
}