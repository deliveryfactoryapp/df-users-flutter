import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BasicWidgetPage {
  Widget getBasicPage({List<Widget> children}) {
    return Scaffold(
      body: Container(
        child: Column(
          children: children,
        ),
      ),
    );
  }
}