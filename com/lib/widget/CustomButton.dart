import 'package:com/Utils/MyColors.dart';
import 'package:com/widget/GeneralText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget{
  bool isLoading;
  double height;
  bool enabled;
  String text;
  Function onTap;


  CustomButton({this.text, this.isLoading, this.height, this.enabled, this.onTap});

  @override
  State<StatefulWidget> createState() => _CustomButton();

}

class _CustomButton extends State<CustomButton>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
        onTap: (widget.enabled) ? widget.onTap : null,
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: (widget.enabled) ? MyColors.greenButton : MyColors.grayText1,
            borderRadius: BorderRadius.circular(8)
          ),
          height: widget.height,
          child: GeneralText().getGeneralText(
            color: Colors.white,
            fontSize: 15,
            fontFamily: FontFamily.regular,
            text: widget.text,
            height: 30
          ),
        ),
      );
  }

  _CustomButton();

}