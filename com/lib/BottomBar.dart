import 'package:flutter/material.dart';

const PAGES = [
  {
    "route": '/myAccount',
    "label": 'Inicio',
    "icon": Icons.home,
    "padding": EdgeInsets.only(left: 0.0),
  },
  {
    "route": '/ListInvoices',
    "label": 'Facturas',
    "icon": Icons.receipt,
    "padding": EdgeInsets.only(right: 40.0),
  },
  {
    "route": '/listProducts',
    "label": 'Catálogo',
    "icon": Icons.style,
    "padding": EdgeInsets.only(left: 35.0),
  },
  {
    "route": '/listClients',
    "label": 'Clientes',
    "icon": Icons.people,
    "padding": EdgeInsets.only(right: 0.0),
  },
];

class BottomBar extends StatelessWidget {
  final currentPage;

  BottomBar(this.currentPage);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Color(0xFF383838),
      shape: CircularNotchedRectangle(),
      notchMargin: 5,
      child: Container(
        height: 65,
        child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: generateOptions(context)
        ),
      ),
    );
  }

  List<Widget> generateOptions(BuildContext context) {
    final List<Widget> items = [];
    for (var i = 0; i < PAGES.length; i++) {
      items.add(
        MaterialButton(
          padding: PAGES[i]['padding'],
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onPressed: () {
            Navigator.of(context).pushNamed(PAGES[i]['route']);
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                PAGES[i]['icon'],
                size: 30,
                color: currentPage == i
                    ? Theme.of(context).primaryColor
                    : Colors.white,
              ),
              Text(
                PAGES[i]['label'],
                style: TextStyle(
                  fontSize: 12,
                  color: currentPage == i
                      ? Theme.of(context).primaryColor
                      : Colors.white,
                ),
              ),
            ],
          ),
        ),
      );
    }
    return items;
  }
}
