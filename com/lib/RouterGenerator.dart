
import 'package:flutter/material.dart';

import 'page/Categories/view/Categorys.dart';
import 'page/Login/view/Login.dart';


class RouterGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => Login());
        break;
      case '/Categories':
        return MaterialPageRoute(builder: (_) {
          return Categories(userData: args);
        });
        break;

    }
  }
}
